package store

import java.util.Date

import akka.actor.ActorSystem
import akka.util.Timeout
import play.api.libs.json.{Json, JsValue}
import store.components.{StorageActor, DbClient, Configuration}
import store.entities.{TimeInsight, TimeResolution, UserInsight}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by Marc Esquerrà on 20/12/2014.
 */
class DataStore(dbClient: DbClient, system: ActorSystem, config: Configuration) {

    val storageActor = system.actorOf(StorageActor.props(dbClient, config))

    /**
     * Saves the provided json in the underlying database
     *
     * @param jsValue
     * @return
     */
    def saveHit(jsValue: JsValue): Option[String] = {

        val useridOpt = (jsValue \ "userId").asOpt[Long]
        val actionOpt = (jsValue \ "action").asOpt[String]

        (useridOpt,actionOpt) match {
            case (Some(userId), Some(action)) =>

                val t = new Date().getTime
                val m = roundToMinute(t).toString
                val h = roundToHour(t).toString

                put(t.toString, Json.stringify(jsValue))
                inc("M" + m)
                inc("H" + h)
                inc("U" + userId.toString)

                None

            case (None, _) => Some("Invalid JSON: the 'userId' field is missing")
            case _         => Some("invalid JSON: the 'action' field is missing")

        }
    }

    def usersInsight(userId: Int): Future[Option[UserInsight]] =
        getLong("U" + userId.toString).map(_.map(UserInsight(userId, _)))

    def timeInsight(timeResolution: TimeResolution, datetime: Long): Future[Option[TimeInsight]] =
        timeResolution match {
            case TimeResolution.Hourly   => getLong("H" + datetime).map(_.map(TimeInsight(timeResolution, datetime, _)))
            case TimeResolution.Minutely => getLong("M" + datetime).map(_.map(TimeInsight(timeResolution, datetime, _)))
        }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private def put(k: String, v: String) =
        storageActor ! StorageActor.Perform{db => db.put(k, v)}

    private def inc(k: String) =
        storageActor ! StorageActor.Perform{db => db.increment(k, 1l)}

    private def getLong(k: String): Future[Option[Long]] = {
        import akka.pattern.ask

        implicit val askTimeout = Timeout.durationToTimeout(config.askTimeout)

        (storageActor ? StorageActor.Perform { db => db.getLong(k)})
            .flatMap{
                case Some(r: Long)   => Future.successful(Some(r))
                case Some(r: Int)    => Future.successful(Some(r.toLong))
                case Some(r: Short)  => Future.successful(Some(r.toLong))
                case None            => Future.successful(None)
                case _               => Future.failed(new Exception("Invalid result type"))
        }

    }

    val MILLIS_PER_MINUTE = 1000 * 60
    val MILLIS_PER_HOUR   = 1000 * 60 * 60

    private def roundToMinute (t: Long): Long =
        ((t / MILLIS_PER_MINUTE) + 1) * MILLIS_PER_MINUTE

    private def roundToHour   (t: Long): Long =
        ((t / MILLIS_PER_HOUR) + 1) * MILLIS_PER_HOUR


}
