package store.entities

import play.api.libs.json._

/**
 * Created by Marc Esquerrà on 21/12/2014.
 */
case class UserInsight(userId: Int, hits: Long)

object UserInsight {
    implicit val userInsightFormat = Json.format[UserInsight]
}