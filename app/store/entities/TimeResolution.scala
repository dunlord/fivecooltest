package store.entities

import play.api.libs.json._

/**
 * Created by Marc Esquerrà on 21/12/2014.
 */
sealed abstract class TimeResolution(val description: String)

object TimeResolution {

    object Minutely  extends TimeResolution("minutely")
    object Hourly    extends TimeResolution("hourly")

    val all = List(Minutely, Hourly)
    val reverse = all.map{tr => (tr.description, tr)}.toMap

    implicit val mediaSizeFormat = new Format[TimeResolution] {
        def writes(size: TimeResolution) = Json.toJson(size.description)
        def reads(json: JsValue)    = json match {
            case JsString("minutely")  => JsSuccess(Minutely)
            case JsString("hourly")    => JsSuccess(Hourly)
            case _                     => JsError()
        }
    }

}
