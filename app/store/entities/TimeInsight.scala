package store.entities

import play.api.libs.json.Json

/**
 * Created by Marc Esquerrà on 21/12/2014.
 */
case class TimeInsight (
                  timeResolution: TimeResolution,
                  datetime:       Long,
                  value:          Long )

object TimeInsight {
    implicit val timeInsightFormat = Json.format[TimeInsight]
}
