package store.components

import scala.concurrent.duration._

/**
 * Created by Marc Esquerrà on 20/12/2014.
 */
class Configuration {

    val maxTrialsBeforeWaiting      = 5
    val waitTimeBeforeRetryingAgain = 10 seconds
    val askTimeout                  = 2 minutes

}
