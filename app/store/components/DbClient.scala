package store.components

import database.scala.DummyDBScala

import scala.concurrent.{ExecutionContext, Future, blocking}
import scala.util.Try

/**
 * Created by Marc Esquerrà on 20/12/2014.
 */
trait DbClient  {

    def put       (key: String, value: String) (implicit ec: ExecutionContext): Future[Unit]
    def get       (key: String)                (implicit ec: ExecutionContext): Future[Option[String]]
    def increment (key: String, amount: Long)  (implicit ec: ExecutionContext): Future[Long]

    def getLong   (key: String)                (implicit ec: ExecutionContext): Future[Option[Long]] =
            get(key).map(o => o.flatMap(s => Try(s.toLong).toOption))
}

class DbClientForDummyDBScala extends DbClient{

    override def put       (key: String, value: String) (implicit ec: ExecutionContext): Future[Unit]           = Future{blocking{DummyDBScala.put(key, value)}}
    override def get       (key: String)                (implicit ec: ExecutionContext): Future[Option[String]] = Future{blocking{DummyDBScala.get(key)}}
    override def increment (key: String, amount: Long)  (implicit ec: ExecutionContext): Future[Long]           = Future{blocking{DummyDBScala.increment(key, amount)}}

}




