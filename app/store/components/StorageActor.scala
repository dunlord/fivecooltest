package store.components

import akka.actor.{Props, ActorRef, ActorLogging, Actor}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import scala.concurrent.duration._

/**
 * Created by Marc Esquerrà on 20/12/2014.
 */
class StorageActor(dbClient: DbClient,
                   config:   Configuration)
        (implicit  ec:       ExecutionContext)      extends Actor
                                                       with ActorLogging {

    import StorageActor._

    override def receive = {
        case Perform(action, retrials) =>
            ensure(action(dbClient), action, retrials + 1)(sender)
    }

    private def ensure[T](result: Future[T], action: Action[T], nextRetrial: Int)(implicit sender: ActorRef = Actor.noSender) =
        result onComplete {
            case Success(r) =>
                log    info    "The DB Action has completed successfully"
                (sender ! r)(self)

            case Failure(t) =>
                log    error    ("The DB Action has FAILED", t)

                if(nextRetrial >= config.maxTrialsBeforeWaiting)  schedule((self ! Perform(action, 0))(sender))
                else                                              (self ! Perform(action, nextRetrial))(sender)
        }

    private def schedule[T](f: => T) = context.system.scheduler.scheduleOnce(config.waitTimeBeforeRetryingAgain)(f)

}

object StorageActor {

    type Action[T] = DbClient => Future[T]

    case class Perform[T](action: Action[T], retrials: Int = 0)

    def props(dbClient: DbClient, config: Configuration)(implicit ec: ExecutionContext): Props = Props(new StorageActor(dbClient, config)(ec))

}


