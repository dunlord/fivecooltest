package controllers

import akka.actor.ActorSystem
import play.api.libs.json.Json
import play.api.mvc._
import store.DataStore
import store.components.{Configuration, DbClientForDummyDBScala}
import store.entities.TimeResolution
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

object Application extends Controller {

    val system    = ActorSystem("datastoreSystem")
    val dbClient  = new DbClientForDummyDBScala()
    val config    = new Configuration()

    val dataStore = new DataStore(dbClient, system, config)

    def hit = Action(BodyParsers.parse.json) { request =>
        dataStore.saveHit(request.body).map(BadRequest(_)).getOrElse(Ok)
    }

    def userInsight(userId: Int) = Action.async{
        dataStore.usersInsight(userId).map{
            case Some(r) => Ok(Json.toJson(r))
            case None    => NotFound
        }
    }

    def timeInsight(timeresolution: String, datetime: Long) = Action.async{
        TimeResolution.reverse.get(timeresolution).map { tr =>
            dataStore.timeInsight(tr, datetime).map {
                case Some(r) => Ok(Json.toJson(r))
                case None    => NotFound
            }
        }.getOrElse(Future.successful(NotAcceptable))
    }
}